import { Component, OnInit, Input } from '@angular/core';
import { Video } from 'src/app/types';
import { VideoDataService } from 'src/app/video-data.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, filter } from 'rxjs/operators';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit {

  video$: Observable<Video>;

  constructor(private svc: VideoDataService, route: ActivatedRoute) {
    this.video$ = route.queryParams
      .pipe(
        map(params => params.selectedId),
        filter(id => !!id),
        switchMap(id => svc.loadVideo(id))
      );
   }

  ngOnInit() {
  }

}
