import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Video } from 'src/app/types';
import { Router } from '@angular/router';


@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {

  @Input() videos;
  @Input() selected: string;



  constructor(private router: Router) { }

  ngOnInit() {
  }

  updateSelection(video: Video) {
    this.router.navigate([], {
      queryParams: {
        selectedId: video.id
      },
      queryParamsHandling: 'merge'
    });
  }

}
