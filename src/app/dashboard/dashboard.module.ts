import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoDashboardComponent } from './video-dashboard/video-dashboard.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { VideoListComponent } from './video-list/video-list.component';
import { StatFiltersComponent } from './stat-filters/stat-filters.component';

import { Routes, RouterModule } from '@angular/router';
import { VideoDisplayComponent } from './video-display/video-display.component';

const routes: Routes = [
  { path: '', component: VideoDashboardComponent, pathMatch: 'full' },
];

@NgModule({
  declarations: [
    VideoDashboardComponent,
    VideoPlayerComponent,
    VideoListComponent,
    StatFiltersComponent,
    VideoDisplayComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
