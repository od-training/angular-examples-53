import { Component, OnInit } from '@angular/core';
import { Video } from 'src/app/types';
import { VideoDataService } from 'src/app/video-data.service';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent  {

  videoList: Observable<Video[]>;
  selected: Observable<string>;

  // localhost:4200/dashboard?selectedId=5
  constructor(svc: VideoDataService, route: ActivatedRoute) {
    this.videoList = svc.loadVideos();
    this.selected = route.queryParams
      .pipe(
        map(params => params.selectedId)
      );

  }

}
