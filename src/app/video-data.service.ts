import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './types';
import { Observable } from 'rxjs';

const API_URL = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) {
  }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(API_URL + '/videos');
  }

  loadVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`${API_URL}/videos/${id}`);
  }
}
